package de.babautzen.baapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import de.babautzen.baapp.canteen.ScheduleCache;
import de.babautzen.baapp.fragments.AboutFragment;
import de.babautzen.baapp.fragments.ContactsFragment;
import de.babautzen.baapp.fragments.EventsFragment;
import de.babautzen.baapp.fragments.FoodmenuFragment;
import de.babautzen.baapp.fragments.HochzwoFragment;
import de.babautzen.baapp.fragments.NewsFragment;
import de.babautzen.baapp.fragments.PartnerFragment;
import de.babautzen.baapp.fragments.SettingsFragment;
import de.babautzen.baapp.fragments.TimetableFragment;
import de.babautzen.baapp.fragments.WebviewFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private Fragment currentOpen;

    private final Handler MAIN_THREAD = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // To start Activiy with FoodMenu Fragment
        if (savedInstanceState == null) {
            SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
            MenuItem initialMenuItem;
            switch (settings.getString("Startpage", "Null")) {
                case "Timetable":
                    initialMenuItem = navigationView.getMenu().findItem(R.id.nav_timetable);
                    break;
                case "News":
                    initialMenuItem = navigationView.getMenu().findItem(R.id.nav_news);
                    break;
                case "Contacts":
                    initialMenuItem = navigationView.getMenu().findItem(R.id.nav_contacts);
                    break;
                case "Events":
                    initialMenuItem = navigationView.getMenu().findItem(R.id.nav_events);
                    break;
                case "Foodmenu": // fall-through
                default:
                    initialMenuItem = navigationView.getMenu().findItem(R.id.nav_foodmenu);
            }

            if (initialMenuItem != null) {
                initialMenuItem.setChecked(true);
                onNavigationItemSelected(initialMenuItem);
            }
        }

        ScheduleCache.registerLoadingJob(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Return false to hide the 3 dots in the top right corner for settings
        // TODO 3 little dots in the right top corner better for UX or better have a settings tab in the "mainmenu"-bar? (set this true to test)
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //single onBackPressed show Toast to
    //double onBackPressed within 3 seconds to close the app
    private long lastBackPress = -1;

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        if (currentOpen instanceof WebviewFragment) {
            final boolean wentBack = ((WebviewFragment) currentOpen).stepBack();
            if (wentBack) {
                return;
            }
        }

        if (lastBackPress < 0 || (System.currentTimeMillis() - lastBackPress) > 3000) {
            Toast.makeText(this, R.string.doubletap_to_close_babzi,
                    Toast.LENGTH_SHORT).show();
            lastBackPress = System.currentTimeMillis();
            return;
        }

        super.onBackPressed();
    }

    private void updateTitle(@NonNull MenuItem current) {
        switch (current.getItemId()) {
            case R.id.nav_news:
                setTitle(getResources().getString(R.string.title_news));
                break;
            case R.id.nav_foodmenu:
                setTitle(getResources().getString(R.string.title_foodmenu));
                break;
            case R.id.nav_timetable:
                setTitle(getResources().getString(R.string.title_timetable));
                break;
            case R.id.nav_contacts:
                setTitle(getResources().getString(R.string.title_contacts));
                break;
            case R.id.nav_settings:
                setTitle(getResources().getString(R.string.title_settings));
                break;
            case R.id.nav_about:
                setTitle(getResources().getString(R.string.title_about));
                break;
            case R.id.nav_events:
                setTitle(getResources().getString(R.string.title_events));
                break;
            case R.id.nav_hochzwo:
                setTitle(getResources().getString(R.string.title_hochzwo));
                break;
            case R.id.nav_partner:
                setTitle(getResources().getString(R.string.title_partner));
                break;
            default:
                setTitle(getResources().getString(R.string.app_name));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        item.setChecked(true);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(item.getItemId());

        Fragment newOpen = null;
        switch (item.getItemId()) {
            case R.id.nav_news:
                newOpen = new NewsFragment();
                break;
            case R.id.nav_foodmenu:
                newOpen = new FoodmenuFragment();
                break;
            case R.id.nav_timetable:
                newOpen = new TimetableFragment();
                break;
            case R.id.nav_events:
                newOpen = new EventsFragment();
                break;
            case R.id.nav_contacts:
                newOpen = new ContactsFragment();
                break;
            case R.id.nav_settings:
                newOpen = new SettingsFragment();
                break;
            case R.id.nav_about:
                newOpen = new AboutFragment();
                break;
            case R.id.nav_hochzwo:
                newOpen = new HochzwoFragment();
                break;
            case R.id.nav_partner:
                newOpen = new PartnerFragment();
                break;
            default:
        }
        if (newOpen != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newOpen).commit();
            currentOpen = newOpen;
        }
        drawer.closeDrawer(GravityCompat.START);

        updateTitle(item);
        return true;
    }
}
