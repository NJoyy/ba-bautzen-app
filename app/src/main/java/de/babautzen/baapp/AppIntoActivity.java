package de.babautzen.baapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AppIntoActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_app_into);

        addSlide(AppIntroFragment.newInstance("Willkommmen","Ba Bautzen Info - kurz babzi sagt Hi!",
                R.drawable.ba_sachsen_icon,ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));

        addSlide(AppIntroFragment.newInstance("Stundenplan","Scanne den QR Code auf der Stundenplan Webseite und erhalte Zugriff zu Deinem Stundenplan",
                R.drawable.timetableintroimage,ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));

        addSlide(AppIntroFragment.newInstance("Speiseplan","Schau Dir an, was es in der Mensa gibt. Du kannst dafür auch das Widget benutzen",
                R.drawable.foodmenueintroimage,ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark)));

        addSlide(AppIntroFragment.newInstance("Termine, Termine, Termine","Was läuft aktuell am Campus? Verpasse nichts mehr!",
                R.drawable.eventsintroimage,ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));

        addSlide(AppIntroFragment.newInstance("Los geht´s!","Hast Du Fragen oder Anregungen? Gern kannst Du uns kontaktieren",
                R.drawable.shareintroimage,ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
