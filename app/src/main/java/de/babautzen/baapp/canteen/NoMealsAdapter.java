package de.babautzen.baapp.canteen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import de.babautzen.baapp.R;

public class NoMealsAdapter extends ArrayAdapter<String> {

    public NoMealsAdapter(@NonNull Context context) {
        super(context, 0);
        add(context.getResources().getString(R.string.no_meal));
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String s = getItem(position);
        if (convertView == null) {
            convertView = new TextView(getContext());
        }
        ((TextView) convertView).setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
        ((TextView) convertView).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        ((TextView) convertView).setText(s);
        return convertView;
    }

}
