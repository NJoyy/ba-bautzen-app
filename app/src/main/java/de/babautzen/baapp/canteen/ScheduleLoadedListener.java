package de.babautzen.baapp.canteen;

/**
 * Listener zum Erwarten des Ladens des RSS-Feeds der Mensa aus einem Hintergrundprozess heraus
 *
 * @author Tim Trense
 */
public interface ScheduleLoadedListener {

    /**
     * Wird aufgerufen, wenn der Speiseplan geladen wurde.
     * ACHTUNG Der Aufruf erfolgt eventuell nicht im Thread von dem der Ladeprozess begonnen wurde
     *
     * @param s der geladene Speiseplan (null bei Fehler)
     */
    void onScheduleLoaded(final Schedule s);

}
