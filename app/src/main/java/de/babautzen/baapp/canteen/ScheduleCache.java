package de.babautzen.baapp.canteen;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Anwendungsweiter Zugriffspunkt auf den Mensa-Speiseplan
 *
 * @author Tim Trense
 */
public class ScheduleCache extends JobService {

    /**
     * Applikationsweite ID für den RSS-Feed-Lade-Job
     */
    public static final int JOB_ID = 1;
    private static final Gson gson = new Gson();
    private Thread currentLoader = null;

    /**
     * @param context der Context von dem der Cache geladen werden soll (typischerweise die App selbst)
     * @return den letzten geladenen Speiseplan, null wenn keiner geladen wurde oder der Speiseplan mittlerweile outdated ist
     */
    public static Schedule loadFromCache(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("de.babautzen.baapp.canteen", MODE_PRIVATE);

        String json = prefs.getString("schedule", null);

        if (json == null || json.isEmpty()) {
            return null;
        }

        try {
            return gson.fromJson(json, Schedule.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Lädt den RSS-Feed und speichert den Speiseplan im lokalen Cache.
     * Folgende Aufrufe zu {@link #loadFromCache()} werden ein gleiches (nicht das identische) Objekt liefern
     *
     * @param context der Context von dem der Cache geladen werden soll (typischerweise die App selbst)
     * @return der geladene Speiseplan, null bei Ladefehler
     */
    public static Schedule loadFromRss(Context context) {
        Schedule s = RssScheduleReader.saveLoadAll("42");
        if (s == null) {
            return null;
        }
        String json = gson.toJson(s);

        SharedPreferences.Editor prefs = context.getSharedPreferences("de.babautzen.baapp.canteen", MODE_PRIVATE).edit();
        prefs.putString("schedule", json);
        prefs.apply();

        return s;
    }

    /**
     * Lädt den RSS-Feed in einem Hintergrundprozess
     *
     * @param context  der Context von dem der Cache geladen werden soll (typischerweise die App selbst)
     * @param callback die aufzurufende Funktion nach dem Laden, wird auch aufgerufen mit null-Argument, wenn beim Laden ein Fehler auftrat
     */
    public static void loadAsyncFromRss(final Context context, final ScheduleLoadedListener callback) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                final Schedule s = loadFromRss(context);
                if (callback != null) {
                    callback.onScheduleLoaded(s);
                }
            }
        };
        t.start();
    }

    /**
     * Trägt diesen RSS-Feed-Lade-Job ins System ein
     *
     * @param context der Context zum Zugriff auf den JobScheduler
     * @return Erfolg beim Eintragen
     */
    @SuppressWarnings("UnusedReturnValue")
    public static boolean registerLoadingJob(Context context) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        if (scheduler == null) {
            return false;
        }

        JobInfo.Builder b = new JobInfo.Builder(JOB_ID, new ComponentName(context, ScheduleCache.class));
        b.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        //b.setPeriodic(1000 * 60 * 60 * 6); // update once every 6 hours
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            b.setMinimumLatency(1000 * 5);
        } else {
            b.setPeriodic(1000 * 5);
        }
        //b.setRequiresDeviceIdle(true);
        JobInfo jobInfo = b.build();

        return scheduler.schedule(jobInfo) == JobScheduler.RESULT_SUCCESS;
    }

    /**
     * Nutzt den Context dieses Service
     *
     * @return den letzten geladenen Speiseplan, null wenn keiner geladen wurde oder der Speiseplan mittlerweile outdated ist
     */
    public Schedule loadFromCache() {
        return loadFromCache(this);
    }

    /**
     * Lädt den RSS-Feed und speichert den Speiseplan im lokalen Cache.
     * Folgende Aufrufe zu {@link #loadFromCache()} werden ein gleiches (nicht das identische) Objekt liefern
     *
     * @return der geladene Speiseplan, null bei Ladefehler
     */
    public Schedule loadFromRss() {
        return loadFromRss(this);
    }

    private void doJob(final JobParameters params) {
        if (currentLoader != null && currentLoader.isAlive()) {
            return;
        }

        currentLoader = new Thread() {
            @Override
            public void run() {

                Schedule s = loadFromRss(ScheduleCache.this);
                Log.d("CanteenSchedule-Loader", "loaded schedule: " + s);

                jobFinished(params, true);

                currentLoader = null;
            }
        };
        currentLoader.start();

    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        doJob(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (currentLoader != null) {
            currentLoader.interrupt();
        }
        return false;
    }

}
