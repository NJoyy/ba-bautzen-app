package de.babautzen.baapp.canteen;

/**
 * Vorausplanungstag für den RSS-Feed der Mensa
 *
 * @author Tim Trense
 */
public enum DayAhead {
    TODAY("heute"),
    TOMORROW("morgen");

    private final String rssName;

    private DayAhead(String rssName) {
        this.rssName = rssName;
    }

    public static DayAhead getByRssName(String rssName) {
        for (DayAhead d : DayAhead.values()) {
            if (d.getRssName().equals(rssName)) {
                return d;
            }
        }
        return null;
    }

    /**
     * @return URL-Parameterwert für den Parameter "tag"
     */
    public String getRssName() {
        return rssName;
    }
}

