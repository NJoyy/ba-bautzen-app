package de.babautzen.baapp.canteen;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.util.Calendar;
import java.util.List;

import de.babautzen.baapp.MainActivity;
import de.babautzen.baapp.R;
import de.babautzen.baapp.fragments.SettingsFragment;

/**
 * Implementation of App Widget functionality.
 */
public class CanteenWidget extends AppWidgetProvider {


    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int appWidgetId : appWidgetIds) {
            // Intent to launch the Activity behind widget
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);


            // Layout for widget and on-click-listener to launch intent
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.canteen_widget);
            views.setOnClickPendingIntent(R.id.widgetLogo, pendingIntent);


            Schedule schedule = ScheduleCache.loadFromCache(context);
            List<Meal> meals;
            String day;

            Calendar now = Calendar.getInstance();
            if (now.get(Calendar.HOUR_OF_DAY) > 13) {
                DailySchedule s = schedule != null ? schedule.getScheduleForDay(DayAhead.TOMORROW) : null;
                meals = s != null ? s.getMeals() : null;
                day = context.getResources().getString(R.string.tomorrow);
            } else {
                DailySchedule s = schedule != null ? schedule.getScheduleForDay(DayAhead.TODAY) : null;
                meals = s != null ? s.getMeals() : null;
                day = context.getResources().getString(R.string.today);
            }

            if (meals == null || meals.isEmpty()) {
                // try loading the other day, if for the selected one is no data available
                if (context.getResources().getString(R.string.today).equals(day)) {
                    DailySchedule s = schedule != null ? schedule.getScheduleForDay(DayAhead.TOMORROW) : null;
                    meals = s != null ? s.getMeals() : null;
                    day = context.getResources().getString(R.string.tomorrow);
                } else {
                    DailySchedule s = schedule != null ? schedule.getScheduleForDay(DayAhead.TODAY) : null;
                    meals = s != null ? s.getMeals() : null;
                    day = context.getResources().getString(R.string.today);
                }
            }

            views.setTextViewText(R.id.widgetHeadline, day);

            if (meals != null && meals.size() > 0) {
                views.setTextViewText(R.id.widgetTextViewMeal1, meals.get(0).getTitle());
                views.setTextViewText(R.id.widgetTextViewPrice1S, meals.get(0).getPriceForStudents() + " €");
                views.setTextViewText(R.id.widgetTextViewPrice1M, meals.get(0).getPriceForEmployees() + " €");
            } else {
                views.setTextViewText(R.id.widgetTextViewMeal1, context.getResources().getString(R.string.no_meal));
                views.setTextViewText(R.id.widgetTextViewPrice1S, context.getResources().getString(R.string.no_meal_price));
                views.setTextViewText(R.id.widgetTextViewPrice1M, context.getResources().getString(R.string.no_meal_price));
            }

            if (meals != null && meals.size() > 1) {
                views.setTextViewText(R.id.widgetTextViewMeal2, meals.get(1).getTitle());
                views.setTextViewText(R.id.widgetTextViewPrice2S, meals.get(1).getPriceForStudents() + " €");
                views.setTextViewText(R.id.widgetTextViewPrice2M, meals.get(1).getPriceForEmployees() + " €");
            } else {
                views.setTextViewText(R.id.widgetTextViewMeal2, context.getResources().getString(R.string.no_meal));
                views.setTextViewText(R.id.widgetTextViewPrice2S, context.getResources().getString(R.string.no_meal_price));
                views.setTextViewText(R.id.widgetTextViewPrice2M, context.getResources().getString(R.string.no_meal_price));
            }

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);

        }

    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetIds);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}
