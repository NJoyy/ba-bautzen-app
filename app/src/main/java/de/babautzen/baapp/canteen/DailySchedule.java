package de.babautzen.baapp.canteen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tagesplan gemäß des RSS-Speiseplan-Feeds
 *
 * @author Tim Trense
 */
public class DailySchedule {
    private List<Meal> meals;

    /**
     * Initialisiert einen Tagesplan mit gegebenen Gerichten
     *
     * @param meals Liste der Tagesgerichte, nullable
     */
    public DailySchedule(List<Meal> meals) {
        if (meals != null) {
            this.meals = meals;
        } else {
            this.meals = new ArrayList<>();
        }
    }

    /**
     * Initialisiert einen Tagesplan mit gegebenen Gerichten
     *
     * @param meals Liste der Tagesgerichte, nullable, omittable
     */
    public DailySchedule(Meal... meals) {
        this.meals = new ArrayList<>();
        if (meals != null && meals.length > 0) {
            this.meals.addAll(Arrays.asList(meals));
        }
    }

    /**
     * @return Liste der Tagesgerichte
     */
    public List<Meal> getMeals() {
        return meals;
    }
}
