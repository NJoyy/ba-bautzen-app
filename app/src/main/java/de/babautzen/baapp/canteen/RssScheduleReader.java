package de.babautzen.baapp.canteen;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

/**
 * RSS-Reader für den Mensa-Speiseplan des Studentenwerks Dresden.
 * Hinweis: MensaID für Bautzen ist "42".
 *
 * @author Tim Trense
 */
public class RssScheduleReader {
    private static final String TEMPLATE_URL = "https://www.studentenwerk-dresden.de/feeds/speiseplan.rss?mid=MENSA_ID&tag=DAY_AHEAD";
    private static final Pattern RSS_MEAL_TITLE_REGEX = Pattern.compile("(.*?) ?(\\((\\d+\\.?\\d*) EUR / (\\d+\\.?\\d*) EUR\\))?");

    /**
     * @param mensaId ID der Mensa, deren RSS-Feed geladen werden soll
     * @param day     Vorausplanungszeitraum der geladen werden soll
     * @return URL des entsprechenden RSS-Feeds
     */
    public static URL getUrl(String mensaId, DayAhead day) throws MalformedURLException {
        String url = TEMPLATE_URL;
        url = url.replace("MENSA_ID", mensaId);
        url = url.replace("DAY_AHEAD", day.getRssName());
        return new URL(url);
    }

    /**
     * fast-forward für loadAll mit try-catch. Liefert null bei Fehler.
     *
     * @param mensaId ID der Mensa, für die alle Tagespläne geladen werden sollen
     * @return liefert den Mehrtagesplan zum aktuellen Zeitpunkt, null bei Fehler
     */
    public static Schedule saveLoadAll(String mensaId) {
        Schedule s = new Schedule();
        DailySchedule tmp = null;
        try {
            tmp = loadDaily(mensaId, DayAhead.TODAY);
        } catch (IOException | XMLStreamException ignored) {
        }
        s.setScheduleForDay(DayAhead.TODAY, tmp);
        tmp = null;
        try {
            tmp = loadDaily(mensaId, DayAhead.TOMORROW);
        } catch (IOException | XMLStreamException ignored) {
        }
        s.setScheduleForDay(DayAhead.TOMORROW, tmp);
        return s;
    }

    /**
     * Lädt den Mehrtagesspeiseplan zum aktuellen Zeitpunkt
     *
     * @param mensaId ID der Mensa, für die der Plan geladen werden soll
     * @return der Mehrtagesplan, nie null, throw bei Fehler
     * @throws IOException        bei Fehlern beim Laden des RSS-Feeds
     * @throws XMLStreamException bei Fehlern beim Parsen des RSS-Feeds
     */
    public static Schedule loadAll(String mensaId) throws IOException, XMLStreamException {
        Schedule s = new Schedule();
        DailySchedule tmp = null;
        tmp = loadDaily(mensaId, DayAhead.TODAY);
        s.setScheduleForDay(DayAhead.TODAY, tmp);
        tmp = null;
        tmp = loadDaily(mensaId, DayAhead.TOMORROW);
        s.setScheduleForDay(DayAhead.TOMORROW, tmp);
        return s;
    }

    /**
     * fast-forward für loadDaily. Lädt den Tagesplan mit try-catch
     *
     * @param mensaId ID der Mensa für die der Tagesplan geladen werden soll
     * @param day     Vorausplanungszeitraum für den der Tagesplan geladen werden soll
     * @return Tagesplan, null bei Fehler
     */
    public static DailySchedule saveLoadDaily(String mensaId, DayAhead day) {
        try {
            return loadDaily(mensaId, day);
        } catch (IOException | XMLStreamException e) {
            return null;
        }
    }

    /**
     * Lädt den Tagesplan aus dem RSS-Feed der Mensa
     *
     * @param mensaId ID der Mensa für den der Tagesplan geladen werden soll
     * @param day     Vorausplanungszeitraum für den der Tagesplan geladen werden soll
     * @return Tagesplan, nie null
     * @throws IOException        bei Fehlern beim Laden des RSS-Feeds
     * @throws XMLStreamException bei Fehlern beim Parsen des RSS-Feeds
     */
    public static DailySchedule loadDaily(String mensaId, DayAhead day) throws IOException, XMLStreamException {
        URL url = null;
        try {
            url = getUrl(mensaId, day);
        } catch (MalformedURLException e) {
            throw new IOException(e);
        }

        InputStream in = url.openStream();

        if (in == null) {
            throw new IOException("Cannot open RSS input stream from " + url.toString());
        }

        XMLInputFactory inputFactory = new com.fasterxml.aalto.stax.InputFactoryImpl();
        XMLEventReader reader = inputFactory.createXMLEventReader(in);
        String temp = "";
        boolean inItem = false;
        List<Meal> meals = new LinkedList<>();
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            // System.out.println("Event: " + event.toString());
            if (event instanceof Characters) {
                temp += event.asCharacters().getData();
            }
            if (event.isStartElement()) {
                temp = "";
                if (event.asStartElement().getName().getLocalPart().equals("item")) {
                    inItem = true;
                }
            }
            if (event.isEndElement()) {
                switch (event.asEndElement().getName().getLocalPart()) {
                    case "title":
                        if (inItem) {
                            Meal m = parseMeal(temp);
                            if (m != null) {
                                meals.add(m);
                            }
                        }
                        break;
                    case "item":
                        inItem = false;
                        break;
                }
            }
        }

        DailySchedule result = new DailySchedule(meals);
        return result;
    }

    /**
     * Parst einen title-Eintrag eines Items des RSS-Feeds zu einem Tagesgericht
     *
     * @param rssTitle Inhalt des Eintrags
     * @return Meal das geparste Tagesgericht (evtl. mit bekanntem oder unbekanntem Preis), null bei Parse-Fehler
     */
    public static Meal parseMeal(String rssTitle) {
        Matcher matcher = RSS_MEAL_TITLE_REGEX.matcher(rssTitle);
        if (!matcher.matches()) {
            return null;
        }
        String title = matcher.group(1);

        String priceStudents = matcher.group(3);
        String priceEmployees = matcher.group(4);

        return new Meal(title, priceStudents, priceEmployees);
    }
}
