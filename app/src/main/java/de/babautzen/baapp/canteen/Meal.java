package de.babautzen.baapp.canteen;

/**
 * Ein Item des RSS-Speiseplan-Feeds. Ein Essen an einem Tages
 *
 * @author Tim Trense
 */
public class Meal {
    private String title;
    private String priceStudents, priceEmployees;

    /**
     * Initialisiert ein Essen mit unbekanntem Preis
     *
     * @param title Name des Essens
     */
    public Meal(String title) {
        this(title, null, null);
    }

    /**
     * Initialisiert ein Essen mit gegebenen Preisen
     *
     * @param title          Name des Essens
     * @param priceStudents  Preisangabe für Studenten (Euro)
     * @param priceEmployees Preisangabe für Bedienstete (Euro)
     */
    public Meal(String title, String priceStudents, String priceEmployees) {
        this.title = title;
        this.priceStudents = priceStudents;
        this.priceEmployees = priceEmployees;
    }

    /**
     * @return Name des Essens
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return Preisangabe für Studenten (Euro)
     */
    public String getPriceForStudents() {
        return priceStudents;
    }

    /**
     * @return Preisangabe für Bedienstete (Euro)
     */
    public String getPriceForEmployees() {
        return priceEmployees;
    }
}

