package de.babautzen.baapp.canteen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.babautzen.baapp.R;

public class MealsAdapter extends ArrayAdapter<Meal> {

    public MealsAdapter(@NonNull Context context, List<Meal> meals) {
        super(context, 0, meals);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Meal meal = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.meal_view, parent, false);

        }

        if (meal != null) {
            ((TextView) convertView.findViewById(R.id.title)).setText(meal.getTitle());
            if(meal.getPriceForStudents() != null && meal.getPriceForEmployees() != null){
            SharedPreferences settings = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
            if (settings.getString("canteenPricesFor", "Students").equals("Students")) {
                    ((TextView) convertView.findViewById(R.id.price)).setText(meal.getPriceForStudents() + "€");
                } else {
                    ((TextView) convertView.findViewById(R.id.price)).setText(meal.getPriceForEmployees() + "€");
                }
            }
            else {
                ((TextView) convertView.findViewById(R.id.price)).setText(R.string.sold_out);

            }
        }

        return convertView;
    }
}
