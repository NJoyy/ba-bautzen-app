package de.babautzen.baapp.canteen;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Mehrtagesplan für Gerichte in der Mensa gemäß des RSS-Speiseplan-Feeds
 *
 * @author Tim Trense
 */
public class Schedule {
    private Map<DayAhead, DailySchedule> schedules;

    /**
     * Initialisiert einen leeren Plan
     */
    public Schedule() {
        this(null);
    }

    /**
     * Initialisiert einen Plan
     *
     * @param initial anfängliche Tagesplan-Einträge
     */
    public Schedule(Map<DayAhead, DailySchedule> initial) {
        schedules = new HashMap<>();
        if (initial != null) {
            schedules.putAll(initial);
        }
    }

    /**
     * @return das zugrundeliegende Mapping von Tagen zu Tagesplänen
     */
    public Map<DayAhead, DailySchedule> getDailySchedules() {
        return schedules;
    }

    /**
     * @param d Tag, für den der Tagesplan zu suchen ist
     * @return entsprechender Tagesplan, null wenn für diesen Tag kein Plan existiert oder bekannt ist
     */
    public DailySchedule getScheduleForDay(DayAhead d) {
        return schedules.get(d);
    }

    /**
     * @param d Tag, für den der Tagesplan zu setzen ist
     * @param s Tagesplan für diesen Tag
     * @return vorher gesetzter Tagesplan für diesen Tag, null wenn keiner existierte oder vorher bekannt war
     */
    public DailySchedule setScheduleForDay(DayAhead d, DailySchedule s) {
        return schedules.put(d, s);
    }

    /**
     * @return liefert die Menge der Tage, für den Tagespläne gesetzt wurden
     */
    public Set<DayAhead> getDays() {
        return schedules.keySet();
    }
}
