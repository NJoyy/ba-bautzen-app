package de.babautzen.baapp.fragments;

import android.webkit.WebView;

import de.babautzen.baapp.R;

/**
 * @author Christoph Kempe
 */

public class ContactsFragment extends WebviewFragment {

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.emptycontacts);
    }

    @Override
    protected String getBaseUrl() {
        return "https://www.ba-bautzen.de/die-akademie/ansprechpartner/";
    }
}
