package de.babautzen.baapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import de.babautzen.baapp.canteen.DayAhead;

/**
 * Created by Tim Trense on 10.04.2019.
 */
public class FoodmenuPager extends FragmentPagerAdapter {

    private final Context context;

    @SuppressWarnings("WeakerAccess")
    public FoodmenuPager(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.context = context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        FoodmenuFragmentDaily fragment = new FoodmenuFragmentDaily();
        Bundle args = new Bundle();
        switch (position) {
            case 0:
                args.putString("Day", "Today");
                break;
            case 1:
                args.putString("Day", "Tomorrow");
                break;
            default:
                return null;
        }
        fragment.setArguments(args);
        return fragment;
    }

}