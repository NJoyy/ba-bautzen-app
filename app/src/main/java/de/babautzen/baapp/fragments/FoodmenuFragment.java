package de.babautzen.baapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import de.babautzen.baapp.R;
import de.babautzen.baapp.canteen.Schedule;
import de.babautzen.baapp.canteen.ScheduleCache;

public class FoodmenuFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_foodmenu, container, false);
        onViewCreated(view, savedInstanceState);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final Schedule schedule = ScheduleCache.loadFromCache(Objects.requireNonNull(getContext()));

        if (schedule == null) {
            return;
        }

        final TabLayout tab = view.findViewById(R.id.foodmenu_tabbing);
        final ViewPager pager = view.findViewById(R.id.foodmenu_pager);
        pager.setAdapter(new FoodmenuPager(getChildFragmentManager(), getContext()));
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));


        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }
        });
    }
}
