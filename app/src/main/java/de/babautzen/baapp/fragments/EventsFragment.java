package de.babautzen.baapp.fragments;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewFragment;

import de.babautzen.baapp.R;

/**
 * @author Tim Trense
 */
public class EventsFragment extends WebviewFragment {
    @Override
    protected String getBaseUrl() {
        return "https://www.ba-bautzen.de/die-akademie/veranstaltungen/";
    }

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.emptyevents);
    }
    }
