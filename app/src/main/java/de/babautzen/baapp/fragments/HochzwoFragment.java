package de.babautzen.baapp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;

import de.babautzen.baapp.R;

/**
 * @author Tim Trense
 */
public class HochzwoFragment extends WebviewFragment {
    @Override
    protected String getBaseUrl() {
        return "https://www.ba-bautzen.de/die-akademie/dokumente/";
    }

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.emptyhochzwo);
    }

    @Override
    protected boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(request.getUrl());
        startActivity(i);
        return true;
    }
}
