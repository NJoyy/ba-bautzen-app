package de.babautzen.baapp.fragments;

import android.webkit.WebView;

import de.babautzen.baapp.R;

/**
 * @author Tim Trense
 */
public class PartnerFragment extends WebviewFragment {
    @Override
    protected String getBaseUrl() {
        return "https://www.ba-bautzen.de/vor-dem-studium/praxispartnersuche/";
    }

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.emptypartner);
    }
}
