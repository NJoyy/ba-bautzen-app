package de.babautzen.baapp.fragments;

import android.webkit.WebView;

import de.babautzen.baapp.R;

/**
 * @author Christoph Kempe
 */

public class NewsFragment extends WebviewFragment {

    @Override
    protected String getBaseUrl() {
        return "https://www.ba-bautzen.de";
    }

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.emptynews);
    }
}
