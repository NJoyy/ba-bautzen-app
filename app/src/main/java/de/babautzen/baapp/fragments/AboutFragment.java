package de.babautzen.baapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.vansuita.materialabout.builder.AboutBuilder;

import de.babautzen.baapp.R;

/**
 * @author Tim Trense
 */
public class AboutFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        final FrameLayout flHolder = view.findViewById(R.id.about);

        AboutBuilder builder = AboutBuilder.with(getContext())
                .setAppIcon(R.drawable.ic_ba_sachsen)
                .setAppName(R.string.app_name)
                .setPhoto(R.drawable.ba_sachsen_icon)
                .setCover(R.mipmap.profile_cover)
                .setLinksAnimated(true)
                .setDividerDashGap(13)
                .setName("16 WI")
                .setSubTitle("Wirtschaftsinformatik")
                .setLinksColumnsCount(4)
                .setBrief("Endlich die erste BA Bautzen App! Helft uns mit euren Vorschlägen die App am Leben zu erhalten und weiter zu entwickeln!")
                .addWebsiteLink("https://www.ba-bautzen.de")
                .addFacebookLink("BABautzen")
                .addYoutubeUserLink("BaBautzen")
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .setActionsColumnsCount(2)
                .addFeedbackAction("developer@ba-bautzen.de")
                .addPrivacyPolicyAction("https://www.ba-bautzen.de/datenschutz/")
                .addChangeLogAction((Intent) null)
                .setWrapScrollView(true)
                .setShowAsCard(true);

        /*
        .addBitbucketLink("")
                .addFacebookLink("")
                .addTwitterLink("")
                .addInstagramLink("")
                .addGooglePlusLink("")
                .addYoutubeChannelLink("")
                .addDribbbleLink("")
                .addLinkedInLink("")
                .addWhatsappLink()
                .addSkypeLink("user")
                .addGoogleLink("user")
                .addAndroidLink("user")
                .addWebsiteLink("site")
                .addRemoveAdsAction((Intent) null)
                .addDonateAction((Intent) null)
                .addUpdateAction()
                .addMoreFromMeAction("")
                .addHelpAction((Intent) null)
         */


        flHolder.addView(builder.build());

        return view;
    }

}
