package de.babautzen.baapp.fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;

import de.babautzen.baapp.R;

/**
 * @author Fabian Mittmann
 */
public class TimetableFragment extends WebviewFragment {

    @Override
    protected String getBaseUrl() {
        //token eg. 16WI = fef57ae48fac6d36cb0b545a2be4b2c4c80b111e28bab1a6

        //noinspection ConstantConditions
        String data = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE).getString("Stundenplantoken", "Null");
        return "https://webapi.ba-bautzen.de/stundenplan/PlanServlet?Format=HTML&data=" + data;
    }

    @Override
    protected void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        executeJsResource(R.raw.check_timetable_unauthorized);
    }

    @Override
    protected boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (request.getUrl().toString().contains("babzi_timetable_unauthorized")) {
            loadHtmlResource(R.raw.timetable_unauthorized);
        } else {
            view.loadUrl(request.getUrl().toString());
        }

        return false;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        super.onDestroyView();
    }

}
