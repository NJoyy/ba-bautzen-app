package de.babautzen.baapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import de.babautzen.baapp.R;

/**
 * @author Christoph Kempe
 */
public class SettingsFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onPause() {
        super.onPause();

        //Einstellungen editiern...
        SharedPreferences.Editor s = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE).edit();
        RadioGroup rg = getView().findViewById(R.id.settings_initial_choosestartpage);
        Switch prices = getView().findViewById(R.id.settings_prices_employees);

        switch (rg.getCheckedRadioButtonId()) {
            case R.id.settings_initial_timetable:
                s.putString("Startpage", "Timetable");
                break;
            case R.id.settings_initial_foodmenu:
                s.putString("Startpage", "Foodmenu");
                break;
            case R.id.settings_initial_news:
                s.putString("Startpage", "News");
                break;
            case R.id.settings_initial_contacts:
                s.putString("Startpage", "Contacts");
                break;
            case R.id.settings_initial_events:
                s.putString("Startpage", "Events");
                break;
        }
        EditText token = getView().findViewById(R.id.settings_token);
        s.putString("Stundenplantoken", token.getText().toString());
        s.putString("canteenPricesFor", prices.isChecked() ? "Employees" : "Students");
        //andere Einstellungen hier->

        //Einstellungen speichern...
        s.apply();

    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onResume() {
        super.onResume();

        //Einstellungen laden...
        SharedPreferences s = this.getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
        String startpage = s.getString("Startpage", "Null");
        RadioGroup rg = getView().findViewById(R.id.settings_initial_choosestartpage);
        Switch prices = getView().findViewById(R.id.settings_prices_employees);

        switch (startpage) {
            case "Timetable":
                rg.check(R.id.settings_initial_timetable);
                break;
            case "Foodmenu":
                rg.check(R.id.settings_initial_foodmenu);
                break;
            case "News":
                rg.check(R.id.settings_initial_news);
                break;
            case "Contacts":
                rg.check(R.id.settings_initial_contacts);
                break;
            case "Events":
                rg.check(R.id.settings_initial_events);
                break;
            case "Null":
                rg.check(R.id.settings_initial_timetable);
                break;
        }
        EditText token = getView().findViewById(R.id.settings_token);
        token.setText(s.getString("Stundenplantoken", ""));
        prices.setChecked(s.getString("canteenPricesFor", "Students").equals("Employees"));

        /*
         * camera imagebutton for scanning the tokenstring - timetable
         */
        ImageButton cameraButton = getView().findViewById(R.id.settings_cameraButton);

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator
                        .forSupportFragment(SettingsFragment.this)
                        .setBeepEnabled(false)
                        .setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
                        .setPrompt(getString(R.string.please_focus_qr_code))
                        .initiateScan();
            }
        });

    }


    /**
     * method comes with zxing library and handles the result of scanned data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        SharedPreferences.Editor s = Objects.requireNonNull(getContext()).getSharedPreferences("settings", Context.MODE_PRIVATE).edit();
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {

            System.out.println(result.getContents());
            s.putString("Stundenplantoken", result.getContents());
            s.apply();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

}







