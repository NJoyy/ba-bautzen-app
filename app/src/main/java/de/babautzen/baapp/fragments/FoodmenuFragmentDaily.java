package de.babautzen.baapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import de.babautzen.baapp.R;
import de.babautzen.baapp.canteen.DailySchedule;
import de.babautzen.baapp.canteen.DayAhead;
import de.babautzen.baapp.canteen.MealsAdapter;
import de.babautzen.baapp.canteen.NoMealsAdapter;
import de.babautzen.baapp.canteen.ScheduleCache;


public class FoodmenuFragmentDaily extends Fragment {

    @SuppressWarnings("ConstantConditions")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_foodmenu_fragment_daily, container, false);

        final ListView list = view.findViewById(R.id.foodmenu_list);

        DailySchedule dailySchedule = null;

        try {
            switch (getArguments().getString("Day", "null")) {
                case "Today":
                    dailySchedule = ScheduleCache.loadFromCache(getContext()).getScheduleForDay(DayAhead.TODAY);
                    break;
                case "Tomorrow":
                    dailySchedule = ScheduleCache.loadFromCache(getContext()).getScheduleForDay(DayAhead.TOMORROW);
                    break;
                default:
                    dailySchedule = null;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (dailySchedule != null && !dailySchedule.getMeals().isEmpty()) {
            MealsAdapter ma = new MealsAdapter(view.getContext(), dailySchedule.getMeals());
            list.setAdapter(ma);
        } else {
            NoMealsAdapter africa = new NoMealsAdapter(view.getContext());
            list.setAdapter(africa);
        }

        return view;
    }
}
