package de.babautzen.baapp.fragments;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import de.babautzen.baapp.R;

/**
 * @author Tim Trense
 */
public abstract class WebviewFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_web, container, false);
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final WebView webview = view.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(getBaseUrl());
        // enable link opening in web view instead of opening in external web browser
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                WebviewFragment.this.onPageFinished(view, url);
                webview.setVisibility(View.VISIBLE);
            }

            //remove header webviews
            @Override
            public void onLoadResource(WebView view, String url) {
                try {
                    webview.loadUrl("javascript:(window.onload = function() {" +
                            "(header = document.getElementById('cc_header')); header.parentNode.removeChild(header);})()");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                WebviewFragment.this.onPageStarted(view, url);
                webview.setVisibility(View.INVISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebviewFragment.this.shouldOverrideUrlLoading(view, request);
            }
        });
    }

    protected abstract String getBaseUrl();

    public void executeJsResource(int id) {
        if (getContext() != null) {
            //noinspection ConstantConditions
            try (InputStream in = getContext().getResources().openRawResource(id)) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                StringBuilder builder = new StringBuilder();
                String buffer = reader.readLine();
                while (buffer != null) {
                    builder.append(buffer);
                    builder.append(System.lineSeparator());
                    buffer = reader.readLine();
                }

                //noinspection ConstantConditions
                ((WebView) getView().findViewById(R.id.webview)).evaluateJavascript(builder.toString(), null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadHtmlResource(int id) {
        if (getContext() != null) {
            //noinspection ConstantConditions
            try (InputStream in = getContext().getResources().openRawResource(id)) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
                StringBuilder builder = new StringBuilder();
                String buffer = reader.readLine();
                while (buffer != null) {
                    builder.append(buffer);
                    builder.append(System.lineSeparator());
                    buffer = reader.readLine();
                }

                //noinspection ConstantConditions
                ((WebView) getView().findViewById(R.id.webview)).loadData(builder.toString(), "text/html", "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void onPageFinished(WebView view, String url) {
        // do nothing
    }

    protected boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        view.loadUrl(request.getUrl().toString()); // open link in this webview
        return false;
    }

    protected void onPageStarted(WebView view, String url) {
        // do nothing
    }

    public boolean stepBack() {
        try {
            //noinspection ConstantConditions
            WebView webView = getView().findViewById(R.id.webview);
            if (webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        } catch (NullPointerException ignored) {
            // do nothing
        }
        return false;
    }
}
