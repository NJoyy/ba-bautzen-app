
var content = document.getElementById("cc_outer").querySelector("main");
if(window.location.pathname.endsWith("praxispartnersuche") || window.location.pathname.endsWith("praxispartnersuche/") ){
    content = content.querySelector(".cc_bg_0:nth-child(2)");
} else {
    content = content.querySelector(".cc_bg_0:nth-child(1)");
}
content = content.querySelector(".container");
content.style.width = "100%";
content.style.maxWidth = "100%";
content.style.borderColor = "white";
var body = document.body;
body.innerHTML = "";
body.style.backgroundColor = "white";
body.appendChild(content);

var elems = content.querySelectorAll(".cc_content_block");
for(var i in elems) {
    elems[i].style["margin-top"] = "1em";
}

elems = content.querySelectorAll(".cc_bg_4");
for(var i in elems) {
    elems[i].style.background = "none";
}

function click(elem) {
    // Do nothing
}